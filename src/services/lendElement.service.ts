import { LendElement } from '../models/LendElement';

export class LendElementService {
    bookList: LendElement[] = [
        {
            name: "Livre 1",
            description: [
                'Description livre 1 ligne 1',
                'Description livre 1 ligne 2'
            ],
            isLend: true,
            type: "book"
        },
        {
            name: "Livre 2",
            description: [
                'Description CD 2 ligne 1',
                'Description CD 2 ligne 2'
            ],
            isLend: false,
            type: "book"
        },
        {
            name: "Livre 3",
            description: [
                'Description CD 3 ligne 1',
                'Description CD 3 ligne 2'
            ],
            isLend: true,
            type: "book"
        }
    ];
    cdList: LendElement[] = [
        {
            name: "CD 1",
            description: [
                'Description CD 1 ligne 1',
                'Description CD 1 ligne 2'
            ],
            isLend: true,
            type: "cd"
        },
        {
            name: "CD 2",
            description: [
                'Description CD 2 ligne 1',
                'Description CD 2 ligne 2'
            ],
            isLend: true,
            type: "cd"
        },
        {
            name: "CD 3",
            description: [
                'Description CD 3 ligne 1',
                'Description CD 3 ligne 2'
            ],
            isLend: false,
            type: "cd"
        }
    ];

    // Méthode initalement prévue pour modifier la propriété isLend du tableau
    lendElement(type:string, index:number) {
        let list: any[];
        (type == "cd") ? list = this.cdList : list = this.bookList;
        list[index].isLend = !list[index].isLend;
    }
    
}