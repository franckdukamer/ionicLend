import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { LendElement } from '../../../models/LendElement';
import { LendElementService } from '../../../services/lendElement.service';

@Component({
  selector: 'page-lend-book',
  templateUrl: 'lend-book.html',
})
export class LendBookPage implements OnInit {

  index: number;
  lendBook: LendElement;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private viewCtrl: ViewController,
              private lendEltService: LendElementService) {
  }

  ngOnInit() {
    this.index = this.navParams.get('index');
    this.lendBook = this.lendEltService.bookList[this.index];
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LendBookPage');
  }
  closeModal() {
    this.viewCtrl.dismiss();
  }
  onLendBook() {
    this.lendBook.isLend = !this.lendBook.isLend;
  }
}
