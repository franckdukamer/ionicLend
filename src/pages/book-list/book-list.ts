import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, MenuController } from 'ionic-angular';
import { LendBookPage } from './lend-book/lend-book';
import { LendElement } from '../../models/LendElement';
import { LendElementService } from '../../services/lendElement.service';

@Component({
  selector: 'page-book-list',
  templateUrl: 'book-list.html',
})
export class BookListPage {

  bookList: LendElement[];

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private modalCtrl: ModalController,
              private lendEltService: LendElementService,
              private menuCtrl: MenuController) {
  }

  //Exécuté après le constructeur et avant que la page soit disponible
  ionViewWillEnter() {
    this.bookList = this.lendEltService.bookList.slice();
  }

  openBook(index: number) {
    this.modalCtrl.create(LendBookPage, {index: index}).present();
  }

  onToggleMenu() {
    this.menuCtrl.open();
  }
}
