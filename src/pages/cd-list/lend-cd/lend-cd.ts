import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { LendElement } from '../../../models/LendElement';
import { LendElementService } from '../../../services/lendElement.service';

@Component({
  selector: 'page-lend-cd',
  templateUrl: 'lend-cd.html',
})
export class LendCdPage implements OnInit {

  index: number;
  lendCd: LendElement;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private viewCtrl: ViewController,
              private lendEltService: LendElementService) {
  }

  ngOnInit() {
    this.index = this.navParams.get('index');
    this.lendCd = this.lendEltService.cdList[this.index];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LendBookPage');
  }
  closeModal() {
    this.viewCtrl.dismiss();
  }
  onLendCd() {
    this.lendCd.isLend = !this.lendCd.isLend;
  }
}
