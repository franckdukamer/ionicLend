import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, MenuController } from 'ionic-angular';
import { LendCdPage } from './lend-cd/lend-cd';
import { LendElement } from '../../models/LendElement';
import { LendElementService } from '../../services/lendElement.service';

@Component({
  selector: 'page-cd-list',
  templateUrl: 'cd-list.html',
})
export class CdListPage {

  cdList: LendElement[];
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private modalCtrl: ModalController,
              private lendEltService: LendElementService,
              private menuCtrl: MenuController) {
  }

  //Exécuté après le constructeur et avant que la page soit disponible
  ionViewWillEnter() {
    this.cdList = this.lendEltService.cdList.slice();
  }

  openCd(index: number) {
    this.modalCtrl.create(LendCdPage, {index : index}).present();
  }

  onToggleMenu() {
    this.menuCtrl.open();
  }

}
