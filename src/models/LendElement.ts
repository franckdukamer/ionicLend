export class LendElement {
    description: string[];
    isLend: boolean;
    type: string;

    constructor(public name: string) {
        this.isLend = true;
    }
}